## Introduction
- processing of meteorological data in general
- goal of this work

## CzechGlobe

### Global Change Research Institute

- what are they observing, where, and why

### Current state

- data collection
- information about datasets
- current work with the data

### Problem

- fully manual

### Existing solutions

- do similar systems already exist

## Analysis and Design
- chapter intro

### User requirements
- list all used requirements (simplify and automatize usual tasks done when validating and repairing data)

### Non-functional requirements
- must use ElasticSearch database
- work with time series with frequency of measurements as small as 20 seconds

### Proposed solution

- data flow diagram
- 3 distinct parts:
  - user interface - visualization and control
  - data processing component
  - data storage

## Implementation

- chapter intro

### Architecture

- diagram
- high level overview of architecture - what each parts does and how parts communicate (airflow, kibana, kibana-plugin, integraion api, ElasticSearch, and PostgreSQL database)
- following sections descibe

### Programming languages and environment

- Python
- usage in airflow, possibility for any tasks

- Javascript
- only possible option for creating Kibana plugins
- React library - suggested by Kibana development team

- Docker
- containerisation
- utilization in the development process
- reasons to use it

### Cooperation with user

- agile

## Implementation - Data storage component

### Requirements for component

- data storage requirements
- only one solution, ElasticSearch

### ElasticSearch
- what is ElasticSearch, what are its strengths

### Data structure

- previously mentioned indices (raw, unit, final)
- ES index with transformation settings
  - why is it useful - full log of transformations used - can be used to determine qcode
- PostgreSQL database for Ariflow
  - used for storage of information about task runs
- ES index with logs from executed tasks, how to browse them


## Implementation - Visualization component

### Requirements for component

- user interface
  - visualization
  - modify data
  - data analysis
- easy to use
- access control

### Possible solutions
- what are our requirements for visualization lool

- what is Kibana
- pros and cons
- extendibility by plugins

- what is Dejavu
- pros and cons

- cluster management tools
- low level API access

- Kibana best suits our needs

### Kibana

- development of plugin
- changes occuring at the time, upgrade to new platform

### Plugin

- general info about plugin

- gets data from ElasticSearch database, descibed in previous chapter
- contacts integration component for configurations and state of data processing, described in later chapter

### User interface

- where to find it (url, location in menu)
- time series visualization as most important component
- describe all controls


## Implementation - Data processing component

### Workflow manager
- what are our requirements for data processing

#### Airflow
- what is Airflow
- pros and cons

#### Prefect
- what is Prefect
- pros and cons

### Data pipeline

- describe all ElasticSearch indices used in data pipeline

#### Transformations

- what are transformations
- implemented as operators in airflow

- detailed description of all implemented transformations:
  - id, shift, scale, constant, remove
  - align to zero
  - compute - allows any custom transformation described by formula
  - missing values - separate section...

#### Filling of missing values

- why is filling of missing values important

- detailed description of all implemented fillers:
  - linear and polynomial interpolation
  - transplantation from another metric


## Implementation - Integration component

### Requirements for component

- glues whole system together
- why do we need integration between Kibana and Airflow

### Possible solutions

### Integration component

- stanalone REST API service

- HTTP-based API

- description of resources/endpoints available

## Testing, deployment an usage

- chapter intro

### Testing

- covered by unit tests where possible
- most of the data processing is too complex - integration tests


### Deployment

- description of deployment in MUNI cluster

### Docker images

- describe all docker images built
  - kibana-dev
  - kibana
  - validation-workflow-dags
  - validation-workflow-api
- containers running in dev/prod mode
  - elasticsearch
  - airflow-postgres
  - kibana
  - airflow
    - in horizontally scalable production mode more complicated: airflow-scheduler, airflow-worker(s), redis
  - validation-workflow-api

### Guides

- list of guides
- where to find them ()

### Performance & scalability

- horizontally scalable - airflow workers
- tested throughput on current deployment

### Usage by CzechGlobe

- issues raised when we released system into testing by CzechGlobe

## Conclusion

- goals reached

### Future

- discuss ways to extend this system, there is a lot of them
