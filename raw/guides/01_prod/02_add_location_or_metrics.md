
# Location and metric management

## Configuration files structure

Configuration of locations and metrics are the most important settings of the system. This configuration determines which files are processed and how each metric is handled. Example configuration file:

```
locations:

# 1. Location definition
  Demo.1:

# 2. Location configuration
    source_format: csv
    source:
      - "demo/1/{{ ds_nodash }}.txt"
    frequency: 1h
    dag_args:
      start_date: 2020-01-01

# 3. Metrics
    metrics:
      - name_raw: Temp@1
        transformation: scale
        transformation_params:
          coefficient: 100
        name_unit: Temperature 10% (%)
        name_final: Temperature 10% (%)
```

1. *Location definition* All keys specified under `locations` section are used as location names.
2. *Location configuration* Has a few parameters:
    - source_format - One of the supported source file formats (csv, tsv, dat). Source format describes how should be the source files read and interpreted.
    - source - List of paths where to look for source files. Paths can use globs - asterisks to match any file and folder names (e.g., `demo/**/monday_*.txt`). Each path should contain a templated parameter (e.g., `{{ ds_nodash }}`) which is replaced with a currently processed date. (For a list of template parameters refer to [airflow macros](https://airflow.apache.org/docs/stable/macros-ref.html))
    - frequency - Frequency of measured values for this location, e.g. `10m`
    - dag_args - custom arguments which are passed to airflow DAG. Supported arguments are `start_date`, `retry_delay`, `retries`, and `schedule_interval`. There is one required argument `start_date` which is the first date of data for this location. Default `schedule_interval` is one day.
3. *Metrics* Last section in the example configuration contains a list of metrics. To make a metric available in each step (`raw`, `unit`, and `final`) you have to set its respective name for this step (`name_raw`, `name_unit`, `name_final`). When no name is given metric will not be present in this step. Single parameter `name` can be used if all three names are the same. Parameters `transformation` and `transformation_params` configure transformation used when converting data from their raw measurement to physical units. When no transformation is specified no transformation is done.

## Add a new location or metric

1. Open your airflow instance. When running a development version locally it should be available at http://localhost:81/admin/.
2. In the top menu access `Admin > Variables`
3. All variables prefixed with `metrics_config_` are loaded as configuration files, you can add a new variable or modify an existing one. Refer to section Configuration files structure for information on how to write location configuration files.
4. After modifying press `Save` and wait about 10 minutes for this change to be applied to all airflow containers, validation-workflow-api, and therefore also Kibana plugin. Alternatively, you can restart containers with `make prod-down; make prod-up`

## Troubleshooting

When you navigate to airflow home you should see there a DAG for all of your defined locations. They should all be enabled by default. But in they are not you have to enable it with the Off/On button for it to start running tasks and processing the data.

If you are missing DAGs for some of the locations look for error messages in red boxes at the top of Airflow home page. Any problems with configuration files would be listed there.
