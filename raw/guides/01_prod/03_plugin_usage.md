
# Validation plugin - basic usage

Validation plugin is accessible in the left hidden menu as the item name `Validation Workflow` under the `CzechGlobe` category. This plugin is the main interface for the whole validation and processing system.

### Exploratory phase
You can find problems in the data by visualizing metrics using the controls in the top row of the user interface. You can compare the behavior of multiple metrics by choosing one of them as primary and one of them as secondary. This will allow quick visual comparison. When you want to observe a long time statistical values of metrics (e.g. average wind speed per each sensor on a single meteorological tower) you should use the visualization capabilities of Kibana and build a dashboard showing such aggregated view. This view can then be reused in the future.

### Data substitution phase
When you find some problematic segments you can modify or fill them with the usage of transformations. You can apply transformation by selecting an interval in the main view and picking one in the `New transformation` section of the right panel. You can then modify the parameters of the used transformation and press `Save` to apply it. Single transformation can be applied at any given point in time to each metric and step separately. Using different a transformation will replace the existing one and the default transformation is Identity -- no operation. It is also possible to modify parameters of existing transformation, however only the selected interval will be affected by those changes.

### Validation phase
After the exploration for doubtful and missing values is finished, and when they are replaced or filled we can mark data as validated. You can do this by selecting an interval in the main view and pressing `Validate` button in the right panel. When all metrics are marked as validated this will also start an export of the data to the final index.

## Troubleshooting

### Could not locate index-pattern
This means that system index patterns were not created at the initialization and you will have to set them up:
1. You can follow the link "click here to re-create it" or find `Index Patterns` in `Stack Management`.
2. Press "Create index pattern" button.
3. Type name of missing index pattern (e.g. `raw`) into `Index pattern name`, make sure there is no `*` at the end. `Next step >`.
4. Select `Time field` "@timestamp".
5. Under Show advanced settings fill `Custom index pattern ID` with the same name (e.g. `raw`).
6. Press `Create index pattern`

### index pattern field missing
This can happen when you add a new location or a metric. You will have to refresh the index patterns. This can be fixed in the Kibana:
1. Find `Stack Management` in the `Management` section of the left menu and access it
2. Go to `Kibana`, `Index Patterns`.
3. Select index pattern based on the step where the problem occurred (raw, raw_fixed, unit, unit_fixed, or final).
4. Press `Refresh field list.` (it has an icon with a circular arrow) button in the top right corner.
