
# Production deployment

Because the system runs in several Docker containers, this setup can be done on practically any server infrastructure. It is possible to deploy containers in several different ways. And this guide covers the most straight-forward one. If your use-case requires a more complex solution it is better to use some alternative way of doing this (e.g. Kubernetes cluster).

## Initial setup

1. Server should have two publicly available ports, in this example 80 (for Kibana) and 81 (for Airflow).
2. Access your server (using ssh).
3. Install docker using approach specific to your system as described in [docker documentation](https://docs.docker.com/get-docker/).
4. Also install docker-compose: [guide](https://docs.docker.com/compose/install/).
5. Check `make` is installed (`make --help`), it should be. If not install it.
6. Navigate to the appropriate location for system files, e.g `cd ~`.
7. Download system core repository:

    ```
    git clone https://gitlab.com/czechglobe/validation-workflow validation-workflow
    ```
8. Enter folder with system core repository `cd validation-workflow`.
9. `make pull-images` to download all required Docker images.
10. `make prod-setup` to initialize databases.
11. `make prod-up` to create and run docker containers.
13. At this point Kibana should start and it should be accessible at the IP of your server at standard port 80 (e.g. `http://localhost/`).
12. Complete a security guide below.

## Security

1. Security setup is based on this [official guide](https://www.elastic.co/guide/en/kibana/current/using-kibana-with-security.html).
2. Enter into an ElasticSearch container, run a command to set passwords for system users and store them somewhere securely, and exit from the container:

    ```
    docker exec -it validation-workflow_elasticsearch_1 bash
    bin/elasticsearch-setup-passwords interactive
    exit
    ```
3. Configuration files for Kibana, ElasticSearch, and Airflow are bind from `prod` folder. You can view and modify them there.
4. In configuration file `kibana.yml` uncomment username and password lines, and put the password you set for `kibana_system` user.
5. Put any random secret string to `xpack.security.encryptionKey`.
6. Enable `xpack.security.enabled` in the ElasticSearch configuration file `elasticsearch.yml`.
6. Change `authenticate` to `True` in section `[webserver]` of configuration file `airflow.cfg`.
7. Restart Kibana and ElasticSearch:

    ```
    docker restart validation-workflow_kibana_1 validation-workflow_elasticsearch_1
    ```
8. Open your Kibana instance in the browser and log in with the user `elastic` and password you set for it.
9. Navigate to `Stack Management > Security > Roles` and create a new role `airflow_system` for airflow, and a role for standard users.
10. Navigate to `Stack Management > Security > Users` and create user `airflow_system` and personal account(s).
11. In any airflow container run a command with correct arguments to create new user(s):

    ```
    docker exec -it validation-workflow_airflow-webserver_1 bash
    airflow create_user
    exit
    ```
11. Open your airflow instance (at port 81, e.g. `http://localhost:81`) and log in.
12. Under `Admin > Connections` find a connection with name  
    `elasticsearch_db` and put user `airflow_system` and your newly created password into appropriate fields, press `Save` to update connection.

## Upgrade to a new version

1. Access your server (using ssh)
2. Navigate to the location where you set your system files, e.g `cd ~/validation-workflow`
3. Upgrade core repository to the latest version with `git pull`. (This is done primarily to update the version of Validation workflow in `Makefile` to the latest value.)
4. `make pull-images` to download docker images versions matching version of Validation workflow specified in `Makefile`
5. `make prod-up` to recreate changed docker containers

## Kubernetes deployment

Sources also contain Kubernetes configuration files which I used for my experimental deployment. They can be used for production deployment on a Kubernetes cluster, but they will probably require some customization to specific cluster (e.g. ingress definitions are missing).

## Troubleshooting

### Docker commands - no permission to the socket
It is possible to receive errors similar to:
```
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock
```
If you do it means your OS user is not allowed to access docker. You have to be a member of group `docker` for docker commands to work correctly. Use `sudo usermod -a -G docker [username]` to add specified user into a docker group. However, this change is applied only after restart of the session so you have to logout and login.

### Image registry - no permission
If you are using a non-public gitlab repository it is also possible to receive this error:
```
Error response from daemon: Get https://registry.gitlab.com/v2/czechglobe/docker-kibana/manifests/7.9.3: denied: access forbidden
```
You need to log in using login command. Provide your GitLab username and password. If you have Two-Factor Authentication enabled you have to use a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) instead of a password:

```
docker login registry.gitlab.com
```
