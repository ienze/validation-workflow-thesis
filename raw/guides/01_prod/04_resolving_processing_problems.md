
# Resolving data processing problems

Sometimes a problem with data processing can occur. You should be able to access the Airflow user interface of your deployment to find and resolve any problems. (When running a development version locally it should be available at `http://localhost:81/admin/`.)

![](airflow-failure-dags.png)

After opening your Airflow interface you should see a list of DAGs. Next to their names are some basic information and circles representing the state of recent tasks and DAG runs. The number in a red circle means there are some DAG runs that failed. You should investigate this further. Enter the failed DAG by clicking on its name.

![](airflow-failure-tasks.png)

A view similar to this will appear. Each circle (at the top) represents a DAG run and each square represents a single task instance. They change color according to their state. You are looking for a red (failed) or orange (up for retry) tasks. Click on the failed task (red square). A window with many buttons will pop up. The first thing you usually want to do is to press `View logs` and read what caused the failure.

![](airflow-failure-clear.png)

If you think this problem can be resolved just by retrying the tasks once more press a button labeled `Clear`. This will remove the state of the selected task and all downstream tasks. This can be changed to also include other tasks by the switch next to the button. The last switch `Failed` is particularly useful for clearing of state of a large amount of failed tasks at once.

When retry does not help there is some bigger problem that needs a more thorough investigation which is out of the scope of this guide.
