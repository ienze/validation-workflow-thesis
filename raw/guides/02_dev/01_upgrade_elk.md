
# Upgrade Elastic stack

1. Change version of elastic stack (`ELK_VERSION`) in `Makefile` to the desired one. Latest releases can be found at a [GitHub](https://github.com/elastic/kibana/releases). It is best to use the same version of the ElasticSearch database and Kibana, so this setting changes both of them at once.
2. Change version of node (`NODE_VERSION`) to one matching your Kibana version. Matching version can be found in the file

    ```
    https://github.com/elastic/kibana/blob/v{ELK_VERSION}/package.json
    ```

    in section `engines > node`. If you try to run build with an incompatible version you get an error similar to this one:

    ```
    error kibana@7.9.3: The engine "node" is incompatible with this module. Expected version "10.22.1". Got "10.19.0"
    ```
3. Change field `version` in a file `kibana.json`  to the same value as `ELK_VERSION` in `Makefile`. Most of the versions are managed globally from `Makefile`, but the build version of the plugin has to be changed manually.
4. Build current versions of all docker images by using the command `make build-all`. This is going to take a while because the Kibana development environment is quite large. After finishing this step you should have new versions of docker images tagged with your ELK_VERSION. E.g.:  
    `registry.gitlab.com/czechglobe/docker-kibana-dev:7.6.2`
5. Run front-end tests with `make kibana-test`.
6. You can also start a development version locally (`make dev-up` and visit [http://localhost:5601/](http://localhost:5601/)) to verify the new version of Kibana does not cause any visual problems. This should be done because representation on screen is not validated by tests in any way.
7. So far all changes made were just local. If you are satisfied with them you can continue with the release and deployment guides to publish these changes.

## Troubleshooting

### Building docker images
```
manifest for node:42.1.1 not found: manifest unknown: manifest unknown
```
This kind of error is shown when the version you are trying to use is not available. The problem can be caused by both `ELK_VERSION` and `NODE_VERSION`.

### Linux permissions
It is possible to receive errors similar to:
```
error An unexpected error occurred: "EACCES: permission denied, unlink '/kibana/plugins/validation-workflow/node_modules/.yarn-integrity'".
```
This means folder `modules/kibana-plugin-validation-workflow` is not writeable for the user with id 1000. Docker containers are all using user 1000 therefore mounted folders have to be owned by this user as well on the host system.

### Chromium problem
When you see this error message
```
/kibana/x-pack/plugins/reporting/chromium/headless_shell-linux/headless_shell: error while loading shared libraries: libnss3.so: cannot open shared object file: No such file or directory
```
you can safely ignore it. It tells you exporting of pdf reports is not going to work properly because the development container does not contain all required dependencies. But this should not affect validation workflow in any way.

### Any other unexpected problems
When the build of a new kibana-dev image fails it is best to check [release notes](https://www.elastic.co/guide/en/kibana/current/release-notes.html) for any breaking changes.
