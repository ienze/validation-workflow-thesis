
# Helper scripts

All scripts helping with development are gathered in a single Makefile file. You need a utility called "make" for them to work. But it is available by default on most systems. When you want to run one of the helpers you have to execute `make <target>` (e.g. `make dev-up`) in the root folder of the validation-workflow repository.

## Controlling docker environments

Development environment:

- *dev-up* - create and start whole system in a development mode
- *dev-up-airflow*, *dev-up-kibana*, *dev-up-api* - create and start only containers related to one of the components
- *dev-setup* - takes care of initialization of both databases
- *dev-down* - stop and remove all containers

\noindent
Similarly for a production environment:

- *prod-up*, *prod-down* - setup and teardown of production containers
- *prod-setup* - run initializations of databases

\noindent
And a test environment:

- *test-up*, *test-down* - setup and teardown of test databases, tests can then be executed with `make test`

It is best not to run multiple environments at once as they can interfere with each other.

## Development

Testing:

- *test* - run all python tests: we first test operators, hooks, DAGs and afterward also integration API and its endpoints
- *kibana-test* - run all javascript tests of the Kibana plugin
- *dev-run-airflow* - start an attached airflow container, command can be used to test airflow tasks manually with inside a attached container;

  ```
  make dev-run-airflow
  airflow test <dag> <task> <execution_date>
  exit
  ```

\noindent
Docker image management:

- *build-all* - build all custom docker images locally
- *release* - push local docker images to remote registry
- *pull-images* - pull built docker images from remote registry
- *k8s-deploy* - deploy the system to a Kubernetes cluster (requires prior configuration and does not work on its own)

## Source code formatting and analysis

- *black* - format python source code in both modules (validation-workflow-dags and validation-workflow-api)
- *pylint* - perform a static code analysis of your python source which looks for style and programming errors
- *kibana-lint* - run formatting and code analysis of the Kibana plugin
