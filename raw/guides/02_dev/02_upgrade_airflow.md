
# Update version of Airflow
1. Change the Airflow version (`AIRFLOW_VERSION`) in Makefile. Our airflow images are based on [puckel/docker-airflow](https://github.com/puckel/docker-airflow) so the list of available versions can be browsed at [Docker Hub](https://hub.docker.com/r/puckel/docker-airflow/tags).
2. Afterwards build current versions of all images by executing the command `make build-all`.
3. Run tests to verify a new version does not cause any problems with our operators and Integration API:

    ```
    make test-up
    make test
    make test-down
    ```

    There should be no failures. Some warnings are acceptable.
4. So far all changes made were just local. If you are satisfied with them you can continue with the release and deployment guides to publish these changes.

## Troubleshooting
When tests fail it most likely means there was some breaking change. You can check the [changelog](https://airflow.apache.org/docs/stable/changelog.html) for any mention of breaking change and whether there are some notes on how to use the new version.

### New versions of airflow missing in puckel/docker-airflow
It is likely that this package will be no longer supported. You should replace it with the new official docker image [apache/airflow](https://hub.docker.com/r/apache/airflow) instead. This replaced image will have to be tested thoroughly before using it in production.
