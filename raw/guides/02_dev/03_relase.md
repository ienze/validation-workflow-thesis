
# Release

A release of a new version is done by building a new version of docker images and uploading them to a remote registry.

1. First increase the version of the system, it is a controlled by a environment variable `VALIDATION_WORKFLOW_VERSION` in a `Makefile`. We follow semantic versioning (`MAJOR.MINOR.PATCH`) where major version is increased for breaking changes while minor and patch versions for new features and fixes.
2. Build the latest versions of images with `make build-all`
3. Use script `make release` to push docker images into container registries of Gitlab repositories.
