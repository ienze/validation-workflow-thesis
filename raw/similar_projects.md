# Airflow (physics) data processing

kibana plugin
@inproceedings{bajer2017building,
  title={Building an IoT data hub with Elasticsearch, Logstash and Kibana},
  author={Bajer, Marcin},
  booktitle={2017 5th International Conference on Future Internet of Things and Cloud Workshops (FiCloudW)},
  pages={63--68},
  year={2017},
  organization={IEEE}
}


@inproceedings{mechev2018fast,
  title={Fast and reproducible lofar workflows with aglow},
  author={Mechev, Alexandar and Oonk, Raymond and Shimwell, Timothy and Plaat, Aske and Intema, Huib and Rottgerin, Huub},
  booktitle={2018 IEEE 14th International Conference on e-Science (e-Science)},
  pages={136--144},
  year={2018},
  organization={IEEE}
}
