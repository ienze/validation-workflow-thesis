# Title (EN)

Tool Supporting Analysis of Meteorology Data

# Title (CZ)

Nástroj pro podporu analýzy dat meteorologických měření

# Keywords

meteorology, time series, meteorological data, sensors, big data, data processing, ElasticSearch, Kibana, Airflow

# Abstract (EN)

This thesis is concerned with an analysis of the current state and design followed by the implementation of a new infrastructure supporting users of a Global Change Research Institute - CzechGlobe. A newly built system allows them to validate and process meteorological data and also enhances the analysis of resulting data. Moreover, the thesis describes a scalable deployment of the whole big data processing infrastructure and its testing.

# Abstract (CZ)

Tato práce se zabývá analýzou současného stavu a návrhem a následnou implementací nové infrastruktury pro uživatele Výzkumného ústavu globální změny - CzechGlobe. Nově vybudovaný systém jim umožňuje ověřovat a zpracovávat meteorologická data a také vylepšuje analýzu výsledných dat. Práce dále popisuje škálovatelné nasazení celé infrastruktury na zpracování velkých dat a její testování.

# Thanks

First and foremost, I would like want to thank my supervisor, RNDr. Tomáš Rebok, Ph.D. for allowing me to work on this project and his guidance during all the phases of work. I am grateful that we managed to communicate reasonably well despite the difficulties.
I would also want to thank the Global Change Research Institute - CzechGlobe and especialy Jan Trusina for discussing existing infrastructure and processes with me in a great detail and making large datasets available.
