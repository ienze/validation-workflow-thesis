import sys

step = sys.argv[1]

for line in sys.stdin:
    # content-html
    if step == "preprocess-content-html":
        if line.startswith("%"):
            line = "<small class='comment'>" + line[1:] + "</small>" + "\n\n"
        if line.startswith("TBA"):
            line = "<div class='comment'><p><strong>To be added later, do not edit</strong></p>"
        if line.startswith("/TBA"):
            line = "</div>"

    # content
    if step == "preprocess-content":
        if line.startswith("%"):
            line = ""
        if line.startswith("TBA"):
            line = "\\iffalse"
        if line.startswith("/TBA"):
            line = "\\fi"

    if step == "postprocess-content":
        if "\\tightlist" in line:
            line = ""
        if line.startswith("\\includegraphics{"):
            line = "\\includegraphics[width=\\textwidth]{raw/" + line[17:]
        if line.startswith("\\chapter{Introduction}"):
            line = "" # it is defined in the main template as non-numbered chapter

    # guides
    if step == "preprocess-guides":
        if line.startswith("%"):
            line = ""

    if step == "postprocess-guides":
        if line.startswith("\\begin{verbatim}"):
            line = "\\begin{lstlisting}\n"
        if line.startswith("\\end{verbatim}"):
            line = "\\end{lstlisting}\n"
        if line.startswith("\\includegraphics{"):
            line = "\\noindent\n\\includegraphics[width=\\textwidth]{raw/" + line[17:]
        if "\\tightlist" in line:
            line = ""
        # if line.startswith("\\section{"):
        #     line = "\\section*" + line[8:]
        if line.startswith("\\subsection{"):
            line = "\\subsection*" + line[11:]
        if line.startswith("\\subsubsection{"):
            line = "\\subsubsection*" + line[14:]

    sys.stdout.write(line)
