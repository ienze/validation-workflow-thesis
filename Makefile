.DEFAULT_GOAL := build
GUIDES_PATH := ../validation-workflow/guides

build-guides:
	cat raw/guides/**/*.md | python docs_process.py preprocess-guides | pandoc -f markdown-auto_identifiers -t latex | python docs_process.py postprocess-guides > guides.tex

build-guides-html:
	mkdir -p ${GUIDES_PATH}
	cp raw/airflow-failure-clear.png ${GUIDES_PATH}/airflow-failure-clear.png
	cp raw/airflow-failure-dags.png ${GUIDES_PATH}/airflow-failure-dags.png
	cp raw/airflow-failure-tasks.png ${GUIDES_PATH}/airflow-failure-tasks.png
	cp raw/style_guides.css ${GUIDES_PATH}/style_guides.css
	cat raw/guides/**/*.md | python docs_process.py preprocess-guides | pandoc --table-of-contents --toc-depth 2 --standalone --css style_guides.css --metadata pagetitle="Validation workflow guides" -f markdown -t html > ${GUIDES_PATH}/guides.html

build-content:
	cat raw/content_long.md | python docs_process.py preprocess-content | pandoc -f markdown-auto_identifiers -t latex --top-level-division=chapter | python docs_process.py postprocess-content > content.tex

build-pdf:
	# render (twice because of list of contents)
	pdflatex fi-pdflatex.tex
	biber fi-pdflatex
	pdflatex fi-pdflatex.tex
	pdflatex fi-pdflatex.tex

build: build-guides build-content build-pdf

clear:
	rm fi-pdflatex.aux || true
	rm fi-pdflatex.bbl || true
	rm fi-pdflatex.bcf || true
	rm fi-pdflatex.blg || true
	rm fi-pdflatex.idx || true
	rm fi-pdflatex.log || true
	rm fi-pdflatex.out || true
	rm fi-pdflatex.pdf || true
	rm fi-pdflatex.run.xml || true
	rm fi-pdflatex.toc || true

.PHONY: build-guides build-content build-pdf build clear
