\section{Production deployment}

Because the system runs in several Docker containers, this setup can be
done on practically any server infrastructure. It is possible to deploy
containers in several different ways. And this guide covers the most
straight-forward one. If your use-case requires a more complex solution
it is better to use some alternative way of doing this (e.g.~Kubernetes
cluster).

\subsection*{Initial setup}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Server should have two publicly available ports, in this example 80
  (for Kibana) and 81 (for Airflow).
\item
  Access your server (using ssh).
\item
  Install docker using approach specific to your system as described in
  \href{https://docs.docker.com/get-docker/}{docker documentation}.
\item
  Also install docker-compose:
  \href{https://docs.docker.com/compose/install/}{guide}.
\item
  Check \texttt{make} is installed (\texttt{make\ -\/-help}), it should
  be. If not install it.
\item
  Navigate to the appropriate location for system files, e.g
  \texttt{cd\ \textasciitilde{}}.
\item
  Download system core repository:

\begin{lstlisting}
git clone https://gitlab.com/czechglobe/validation-workflow validation-workflow
\end{lstlisting}
\item
  Enter folder with system core repository
  \texttt{cd\ validation-workflow}.
\item
  \texttt{make\ pull-images} to download all required Docker images.
\item
  \texttt{make\ prod-setup} to initialize databases.
\item
  \texttt{make\ prod-up} to create and run docker containers.
\item
  At this point Kibana should start and it should be accessible at the
  IP of your server at standard port 80
  (e.g.~\texttt{http://localhost/}).
\item
  Complete a security guide below.
\end{enumerate}

\subsection*{Security}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Security setup is based on this
  \href{https://www.elastic.co/guide/en/kibana/current/using-kibana-with-security.html}{official
  guide}.
\item
  Enter into an ElasticSearch container, run a command to set passwords
  for system users and store them somewhere securely, and exit from the
  container:

\begin{lstlisting}
docker exec -it validation-workflow_elasticsearch_1 bash
bin/elasticsearch-setup-passwords interactive
exit
\end{lstlisting}
\item
  Configuration files for Kibana, ElasticSearch, and Airflow are bind
  from \texttt{prod} folder. You can view and modify them there.
\item
  In configuration file \texttt{kibana.yml} uncomment username and
  password lines, and put the password you set for
  \texttt{kibana\_system} user.
\item
  Put any random secret string to \texttt{xpack.security.encryptionKey}.
\item
  Enable \texttt{xpack.security.enabled} in the ElasticSearch
  configuration file \texttt{elasticsearch.yml}.
\item
  Change \texttt{authenticate} to \texttt{True} in section
  \texttt{{[}webserver{]}} of configuration file \texttt{airflow.cfg}.
\item
  Restart Kibana and ElasticSearch:

\begin{lstlisting}
docker restart validation-workflow_kibana_1 validation-workflow_elasticsearch_1
\end{lstlisting}
\item
  Open your Kibana instance in the browser and log in with the user
  \texttt{elastic} and password you set for it.
\item
  Navigate to
  \texttt{Stack\ Management\ \textgreater{}\ Security\ \textgreater{}\ Roles}
  and create a new role \texttt{airflow\_system} for airflow, and a role
  for standard users.
\item
  Navigate to
  \texttt{Stack\ Management\ \textgreater{}\ Security\ \textgreater{}\ Users}
  and create user \texttt{airflow\_system} and personal account(s).
\item
  In any airflow container run a command with correct arguments to
  create new user(s):

\begin{lstlisting}
docker exec -it validation-workflow_airflow-webserver_1 bash
airflow create_user
exit
\end{lstlisting}
\item
  Open your airflow instance (at port 81,
  e.g.~\texttt{http://localhost:81}) and log in.
\item
  Under \texttt{Admin\ \textgreater{}\ Connections} find a connection
  with name\\
  \texttt{elasticsearch\_db} and put user \texttt{airflow\_system} and
  your newly created password into appropriate fields, press
  \texttt{Save} to update connection.
\end{enumerate}

\subsection*{Upgrade to a new version}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Access your server (using ssh)
\item
  Navigate to the location where you set your system files, e.g
  \texttt{cd\ \textasciitilde{}/validation-workflow}
\item
  Upgrade core repository to the latest version with \texttt{git\ pull}.
  (This is done primarily to update the version of Validation workflow
  in \texttt{Makefile} to the latest value.)
\item
  \texttt{make\ pull-images} to download docker images versions matching
  version of Validation workflow specified in \texttt{Makefile}
\item
  \texttt{make\ prod-up} to recreate changed docker containers
\end{enumerate}

\subsection*{Kubernetes deployment}

Sources also contain Kubernetes configuration files which I used for my
experimental deployment. They can be used for production deployment on a
Kubernetes cluster, but they will probably require some customization to
specific cluster (e.g.~ingress definitions are missing).

\subsection*{Troubleshooting}

\subsubsection*{Docker commands - no permission to the socket}

It is possible to receive errors similar to:

\begin{lstlisting}
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock
\end{lstlisting}

If you do it means your OS user is not allowed to access docker. You
have to be a member of group \texttt{docker} for docker commands to work
correctly. Use \texttt{sudo\ usermod\ -a\ -G\ docker\ {[}username{]}} to
add specified user into a docker group. However, this change is applied
only after restart of the session so you have to logout and login.

\subsubsection*{Image registry - no permission}

If you are using a non-public gitlab repository it is also possible to
receive this error:

\begin{lstlisting}
Error response from daemon: Get https://registry.gitlab.com/v2/czechglobe/docker-kibana/manifests/7.9.3: denied: access forbidden
\end{lstlisting}

You need to log in using login command. Provide your GitLab username and
password. If you have Two-Factor Authentication enabled you have to use
a
\href{https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html}{Personal
Access Token} instead of a password:

\begin{lstlisting}
docker login registry.gitlab.com
\end{lstlisting}

\section{Location and metric management}

\subsection*{Configuration files structure}

Configuration of locations and metrics are the most important settings
of the system. This configuration determines which files are processed
and how each metric is handled. Example configuration file:

\begin{lstlisting}
locations:

# 1. Location definition
  Demo.1:

# 2. Location configuration
    source_format: csv
    source:
      - "demo/1/{{ ds_nodash }}.txt"
    frequency: 1h
    dag_args:
      start_date: 2020-01-01

# 3. Metrics
    metrics:
      - name_raw: Temp@1
        transformation: scale
        transformation_params:
          coefficient: 100
        name_unit: Temperature 10% (%)
        name_final: Temperature 10% (%)
\end{lstlisting}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  \emph{Location definition} All keys specified under \texttt{locations}
  section are used as location names.
\item
  \emph{Location configuration} Has a few parameters:

  \begin{itemize}
  \item
    source\_format - One of the supported source file formats (csv, tsv,
    dat). Source format describes how should be the source files read
    and interpreted.
  \item
    source - List of paths where to look for source files. Paths can use
    globs - asterisks to match any file and folder names (e.g.,
    \texttt{demo/**/monday\_*.txt}). Each path should contain a
    templated parameter (e.g., \texttt{\{\{\ ds\_nodash\ \}\}}) which is
    replaced with a currently processed date. (For a list of template
    parameters refer to
    \href{https://airflow.apache.org/docs/stable/macros-ref.html}{airflow
    macros})
  \item
    frequency - Frequency of measured values for this location,
    e.g.~\texttt{10m}
  \item
    dag\_args - custom arguments which are passed to airflow DAG.
    Supported arguments are \texttt{start\_date}, \texttt{retry\_delay},
    \texttt{retries}, and \texttt{schedule\_interval}. There is one
    required argument \texttt{start\_date} which is the first date of
    data for this location. Default \texttt{schedule\_interval} is one
    day.
  \end{itemize}
\item
  \emph{Metrics} Last section in the example configuration contains a
  list of metrics. To make a metric available in each step
  (\texttt{raw}, \texttt{unit}, and \texttt{final}) you have to set its
  respective name for this step (\texttt{name\_raw},
  \texttt{name\_unit}, \texttt{name\_final}). When no name is given
  metric will not be present in this step. Single parameter
  \texttt{name} can be used if all three names are the same. Parameters
  \texttt{transformation} and \texttt{transformation\_params} configure
  transformation used when converting data from their raw measurement to
  physical units. When no transformation is specified no transformation
  is done.
\end{enumerate}

\subsection*{Add a new location or metric}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Open your airflow instance. When running a development version locally
  it should be available at http://localhost:81/admin/.
\item
  In the top menu access \texttt{Admin\ \textgreater{}\ Variables}
\item
  All variables prefixed with \texttt{metrics\_config\_} are loaded as
  configuration files, you can add a new variable or modify an existing
  one. Refer to section Configuration files structure for information on
  how to write location configuration files.
\item
  After modifying press \texttt{Save} and wait about 10 minutes for this
  change to be applied to all airflow containers,
  validation-workflow-api, and therefore also Kibana plugin.
  Alternatively, you can restart containers with
  \texttt{make\ prod-down;\ make\ prod-up}
\end{enumerate}

\subsection*{Troubleshooting}

When you navigate to airflow home you should see there a DAG for all of
your defined locations. They should all be enabled by default. But in
they are not you have to enable it with the Off/On button for it to
start running tasks and processing the data.

If you are missing DAGs for some of the locations look for error
messages in red boxes at the top of Airflow home page. Any problems with
configuration files would be listed there.

\section{Validation plugin - basic usage}

Validation plugin is accessible in the left hidden menu as the item name
\texttt{Validation\ Workflow} under the \texttt{CzechGlobe} category.
This plugin is the main interface for the whole validation and
processing system.

\subsubsection*{Exploratory phase}

You can find problems in the data by visualizing metrics using the
controls in the top row of the user interface. You can compare the
behavior of multiple metrics by choosing one of them as primary and one
of them as secondary. This will allow quick visual comparison. When you
want to observe a long time statistical values of metrics (e.g.~average
wind speed per each sensor on a single meteorological tower) you should
use the visualization capabilities of Kibana and build a dashboard
showing such aggregated view. This view can then be reused in the
future.

\subsubsection*{Data substitution phase}

When you find some problematic segments you can modify or fill them with
the usage of transformations. You can apply transformation by selecting
an interval in the main view and picking one in the
\texttt{New\ transformation} section of the right panel. You can then
modify the parameters of the used transformation and press \texttt{Save}
to apply it. Single transformation can be applied at any given point in
time to each metric and step separately. Using different a
transformation will replace the existing one and the default
transformation is Identity -- no operation. It is also possible to
modify parameters of existing transformation, however only the selected
interval will be affected by those changes.

\subsubsection*{Validation phase}

After the exploration for doubtful and missing values is finished, and
when they are replaced or filled we can mark data as validated. You can
do this by selecting an interval in the main view and pressing
\texttt{Validate} button in the right panel. When all metrics are marked
as validated this will also start an export of the data to the final
index.

\subsection*{Troubleshooting}

\subsubsection*{Could not locate index-pattern}

This means that system index patterns were not created at the
initialization and you will have to set them up: 1. You can follow the
link ``click here to re-create it'' or find \texttt{Index\ Patterns} in
\texttt{Stack\ Management}. 2. Press ``Create index pattern'' button. 3.
Type name of missing index pattern (e.g.~\texttt{raw}) into
\texttt{Index\ pattern\ name}, make sure there is no \texttt{*} at the
end. \texttt{Next\ step\ \textgreater{}}. 4. Select \texttt{Time\ field}
``@timestamp''. 5. Under Show advanced settings fill
\texttt{Custom\ index\ pattern\ ID} with the same name
(e.g.~\texttt{raw}). 6. Press \texttt{Create\ index\ pattern}

\subsubsection*{index pattern field missing}

This can happen when you add a new location or a metric. You will have
to refresh the index patterns. This can be fixed in the Kibana: 1. Find
\texttt{Stack\ Management} in the \texttt{Management} section of the
left menu and access it 2. Go to \texttt{Kibana},
\texttt{Index\ Patterns}. 3. Select index pattern based on the step
where the problem occurred (raw, raw\_fixed, unit, unit\_fixed, or
final). 4. Press \texttt{Refresh\ field\ list.} (it has an icon with a
circular arrow) button in the top right corner.

\section{Resolving data processing problems}

Sometimes a problem with data processing can occur. You should be able
to access the Airflow user interface of your deployment to find and
resolve any problems. (When running a development version locally it
should be available at \texttt{http://localhost:81/admin/}.)

\noindent
\includegraphics[width=\textwidth]{raw/airflow-failure-dags.png}

After opening your Airflow interface you should see a list of DAGs. Next
to their names are some basic information and circles representing the
state of recent tasks and DAG runs. The number in a red circle means
there are some DAG runs that failed. You should investigate this
further. Enter the failed DAG by clicking on its name.

\noindent
\includegraphics[width=\textwidth]{raw/airflow-failure-tasks.png}

A view similar to this will appear. Each circle (at the top) represents
a DAG run and each square represents a single task instance. They change
color according to their state. You are looking for a red (failed) or
orange (up for retry) tasks. Click on the failed task (red square). A
window with many buttons will pop up. The first thing you usually want
to do is to press \texttt{View\ logs} and read what caused the failure.

\noindent
\includegraphics[width=\textwidth]{raw/airflow-failure-clear.png}

If you think this problem can be resolved just by retrying the tasks
once more press a button labeled \texttt{Clear}. This will remove the
state of the selected task and all downstream tasks. This can be changed
to also include other tasks by the switch next to the button. The last
switch \texttt{Failed} is particularly useful for clearing of state of a
large amount of failed tasks at once.

When retry does not help there is some bigger problem that needs a more
thorough investigation which is out of the scope of this guide.

\section{Upgrade Elastic stack}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Change version of elastic stack (\texttt{ELK\_VERSION}) in
  \texttt{Makefile} to the desired one. Latest releases can be found at
  a \href{https://github.com/elastic/kibana/releases}{GitHub}. It is
  best to use the same version of the ElasticSearch database and Kibana,
  so this setting changes both of them at once.
\item
  Change version of node (\texttt{NODE\_VERSION}) to one matching your
  Kibana version. Matching version can be found in the file

\begin{lstlisting}
https://github.com/elastic/kibana/blob/v{ELK_VERSION}/package.json
\end{lstlisting}

  in section \texttt{engines\ \textgreater{}\ node}. If you try to run
  build with an incompatible version you get an error similar to this
  one:

\begin{lstlisting}
error kibana@7.9.3: The engine "node" is incompatible with this module. Expected version "10.22.1". Got "10.19.0"
\end{lstlisting}
\item
  Change field \texttt{version} in a file \texttt{kibana.json} to the
  same value as \texttt{ELK\_VERSION} in \texttt{Makefile}. Most of the
  versions are managed globally from \texttt{Makefile}, but the build
  version of the plugin has to be changed manually.
\item
  Build current versions of all docker images by using the command
  \texttt{make\ build-all}. This is going to take a while because the
  Kibana development environment is quite large. After finishing this
  step you should have new versions of docker images tagged with your
  ELK\_VERSION. E.g.:\\
  \texttt{registry.gitlab.com/czechglobe/docker-kibana-dev:7.6.2}
\item
  Run front-end tests with \texttt{make\ kibana-test}.
\item
  You can also start a development version locally
  (\texttt{make\ dev-up} and visit \url{http://localhost:5601/}) to
  verify the new version of Kibana does not cause any visual problems.
  This should be done because representation on screen is not validated
  by tests in any way.
\item
  So far all changes made were just local. If you are satisfied with
  them you can continue with the release and deployment guides to
  publish these changes.
\end{enumerate}

\subsection*{Troubleshooting}

\subsubsection*{Building docker images}

\begin{lstlisting}
manifest for node:42.1.1 not found: manifest unknown: manifest unknown
\end{lstlisting}

This kind of error is shown when the version you are trying to use is
not available. The problem can be caused by both \texttt{ELK\_VERSION}
and \texttt{NODE\_VERSION}.

\subsubsection*{Linux permissions}

It is possible to receive errors similar to:

\begin{lstlisting}
error An unexpected error occurred: "EACCES: permission denied, unlink '/kibana/plugins/validation-workflow/node_modules/.yarn-integrity'".
\end{lstlisting}

This means folder \texttt{modules/kibana-plugin-validation-workflow} is
not writeable for the user with id 1000. Docker containers are all using
user 1000 therefore mounted folders have to be owned by this user as
well on the host system.

\subsubsection*{Chromium problem}

When you see this error message

\begin{lstlisting}
/kibana/x-pack/plugins/reporting/chromium/headless_shell-linux/headless_shell: error while loading shared libraries: libnss3.so: cannot open shared object file: No such file or directory
\end{lstlisting}

you can safely ignore it. It tells you exporting of pdf reports is not
going to work properly because the development container does not
contain all required dependencies. But this should not affect validation
workflow in any way.

\subsubsection*{Any other unexpected problems}

When the build of a new kibana-dev image fails it is best to check
\href{https://www.elastic.co/guide/en/kibana/current/release-notes.html}{release
notes} for any breaking changes.

\section{Update version of Airflow}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Change the Airflow version (\texttt{AIRFLOW\_VERSION}) in Makefile.
  Our airflow images are based on
  \href{https://github.com/puckel/docker-airflow}{puckel/docker-airflow}
  so the list of available versions can be browsed at
  \href{https://hub.docker.com/r/puckel/docker-airflow/tags}{Docker
  Hub}.
\item
  Afterwards build current versions of all images by executing the
  command \texttt{make\ build-all}.
\item
  Run tests to verify a new version does not cause any problems with our
  operators and Integration API:

\begin{lstlisting}
make test-up
make test
make test-down
\end{lstlisting}

  There should be no failures. Some warnings are acceptable.
\item
  So far all changes made were just local. If you are satisfied with
  them you can continue with the release and deployment guides to
  publish these changes.
\end{enumerate}

\subsection*{Troubleshooting}

When tests fail it most likely means there was some breaking change. You
can check the
\href{https://airflow.apache.org/docs/stable/changelog.html}{changelog}
for any mention of breaking change and whether there are some notes on
how to use the new version.

\subsubsection*{New versions of airflow missing in puckel/docker-airflow}

It is likely that this package will be no longer supported. You should
replace it with the new official docker image
\href{https://hub.docker.com/r/apache/airflow}{apache/airflow} instead.
This replaced image will have to be tested thoroughly before using it in
production.

\section{Release}

A release of a new version is done by building a new version of docker
images and uploading them to a remote registry.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  First increase the version of the system, it is a controlled by a
  environment variable \texttt{VALIDATION\_WORKFLOW\_VERSION} in a
  \texttt{Makefile}. We follow semantic versioning
  (\texttt{MAJOR.MINOR.PATCH}) where major version is increased for
  breaking changes while minor and patch versions for new features and
  fixes.
\item
  Build the latest versions of images with \texttt{make\ build-all}
\item
  Use script \texttt{make\ release} to push docker images into container
  registries of Gitlab repositories.
\end{enumerate}

\section{Helper scripts}

All scripts helping with development are gathered in a single Makefile
file. You need a utility called ``make'' for them to work. But it is
available by default on most systems. When you want to run one of the
helpers you have to execute
\texttt{make\ \textless{}target\textgreater{}}
(e.g.~\texttt{make\ dev-up}) in the root folder of the
validation-workflow repository.

\subsection*{Controlling docker environments}

Development environment:

\begin{itemize}
\item
  \emph{dev-up} - create and start whole system in a development mode
\item
  \emph{dev-up-airflow}, \emph{dev-up-kibana}, \emph{dev-up-api} -
  create and start only containers related to one of the components
\item
  \emph{dev-setup} - takes care of initialization of both databases
\item
  \emph{dev-down} - stop and remove all containers
\end{itemize}

\noindent Similarly for a production environment:

\begin{itemize}
\item
  \emph{prod-up}, \emph{prod-down} - setup and teardown of production
  containers
\item
  \emph{prod-setup} - run initializations of databases
\end{itemize}

\noindent And a test environment:

\begin{itemize}
\item
  \emph{test-up}, \emph{test-down} - setup and teardown of test
  databases, tests can then be executed with \texttt{make\ test}
\end{itemize}

It is best not to run multiple environments at once as they can
interfere with each other.

\subsection*{Development}

Testing:

\begin{itemize}
\item
  \emph{test} - run all python tests: we first test operators, hooks,
  DAGs and afterward also integration API and its endpoints
\item
  \emph{kibana-test} - run all javascript tests of the Kibana plugin
\item
  \emph{dev-run-airflow} - start an attached airflow container, command
  can be used to test airflow tasks manually with inside a attached
  container;

\begin{lstlisting}
make dev-run-airflow
airflow test <dag> <task> <execution_date>
exit
\end{lstlisting}
\end{itemize}

\noindent Docker image management:

\begin{itemize}
\item
  \emph{build-all} - build all custom docker images locally
\item
  \emph{release} - push local docker images to remote registry
\item
  \emph{pull-images} - pull built docker images from remote registry
\item
  \emph{k8s-deploy} - deploy the system to a Kubernetes cluster
  (requires prior configuration and does not work on its own)
\end{itemize}

\subsection*{Source code formatting and analysis}

\begin{itemize}
\item
  \emph{black} - format python source code in both modules
  (validation-workflow-dags and validation-workflow-api)
\item
  \emph{pylint} - perform a static code analysis of your python source
  which looks for style and programming errors
\item
  \emph{kibana-lint} - run formatting and code analysis of the Kibana
  plugin
\end{itemize}
